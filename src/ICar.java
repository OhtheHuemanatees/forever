/**
 * Different types in Java:
 * class
 * enum
 * primitives
 * interfaces\\A class is a combination of data and methods
 * 
 * an interface only defines methods that it's subclasses will have by definition, everything in an interface is public
 * @author unouser
 *
 */
public interface ICar 
{
	String getMake();
	String getModel();
	int getYear();
	int getMileage();
}
