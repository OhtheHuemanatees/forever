/**
 * common workflow in Java:
 * 
 * -Create an interface(lets us program in general)
 * -create an abstract class(Lets us use code share functionality)
 * -Create concrete classes
 * 
 * Abstract:
 * -Cannot be instantiated
 * -Concrete classes that inherit from abstract classes HAVE TO implement all the abstract methods
 * -Abstract subclasses may or may not implement the abstract methods
 * 
 * -Everything extends Object
 * 
 * @author unouser
 *
 */
public abstract class ACar implements ICar, java.io.Serializable{
	
	/** the make of the car*/
	private String make;
	
	/** the model of the car*/
	private String model;
	
	/** the year of the car*/
	private int year;
	
	/** the mileage of the car*/
	private int mileage;
	/**
	 * The constructor for an abstract car
	 * @param inMake The make of the car
	 * @param inModel The model of the car
	 * @param inYear The year of the car
	 */
	public ACar(String inMake, String inModel, int inYear) {
		make = inMake;
		model = inModel;
		year = inYear;
	}
	
	protected void setMileage(int inMileage)
	{
		mileage = inMileage;
	}
	
	@Override
	public String getMake()
	{
		return make;
	}
	
	@Override
	public String getModel() {
		return model;
	}
	
	@Override
	public int getMileage() {
		return mileage;
	}
	
	@Override
	public int getYear() {
		return year;
	}

	@Override
	public String toString() {
		
		return getMake() +  " " + getModel();
	}
	
	
}
